#!/usr/bin/env python
""" flask test app """
# -*- coding: utf-8 -*-

import re
from pathlib import Path

from flask import \
        Flask, \
        jsonify, \
        abort, \
        make_response, \
        request, \
        url_for

from flask_httpauth import \
        HTTPBasicAuth

from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
auth = HTTPBasicAuth()
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///hello.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class Items(db.Model):
    """ items """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32), unique=True, nullable=False)
    desc = db.Column(db.String(140), unique=True, nullable=False)
    done = db.Column(db.Boolean, unique=False, nullable=False)

    def __repr__(self):
        items = [{
            'id': self.id,
            'name': self.name,
            'desc': self.desc,
            'done': self.done
            }]
#        print(items)
        return items

def db_init():
    """ init the DB """
    dbfile = Path(app.config['SQLALCHEMY_DATABASE_URI'])
    if dbfile.is_file() is False:
        db.create_all()

# AUTH
@auth.get_password
def get_password(username):
    """ auth """
    if username == 'base':
        return 'lab'
    return None

# LIST
def make_public_item(item):
    """ url instead of id """
    new_item = {}
    for field in item:
        if field == 'id':
            new_item['uri'] = url_for('get_oneitem', item_id=item['id'], _external=True)
        else:
            new_item[field] = item[field]
    return new_item

@app.route('/items', methods=['GET'])
@auth.login_required
def get_items():
    """ get items """
    items = Items.query.all()
#    return jsonify({'items': [make_public_item(i) for i in items]})
    return jsonify({'items': [i for i in items]})

# GET ONE
@app.route('/items/<int:item_id>', methods=['GET'])
@auth.login_required
def get_oneitem(item_id):
    """ get item by id """
    items = Items.query.all()
    return jsonify({'item': [make_public_item(i) for i in items \
            if i['id'] == item_id]})

# GET DONE ITEMS
@app.route('/items/d=<is_done>', methods=['GET'])
@auth.login_required
def get_done(is_done):
    """ get item by done """
    if is_done == "true":
        return jsonify({'items': [make_public_item(i) for i in items \
                if i['done'] is True]})
    elif is_done == "false":
        return jsonify({'items': [make_public_item(i) for i in items \
                if i['done'] is False]})
    else:
        return jsonify({'items': []})

# GET ITEM BY TITLE
@app.route('/items/t=<item_title>', methods=['GET'])
@auth.login_required
def get_bytitle(item_title):
    """ get item by title """
    return jsonify({'items': [make_public_item(i) for i in items \
            if i['title'] == item_title]})

# SEARCH ITEM BY DESCRIPTION
@app.route(r"/items/s=<pattern>", methods=['GET'])
@auth.login_required
def get_bydesc(pattern):
    """ search item """
    regex = re.compile(r".*%s.*"%re.escape(pattern), re.IGNORECASE)
    return jsonify({'items': [make_public_item(i) for i in items \
            if regex.match(i['description'])]})

# ADD
@app.route('/items', methods=['POST'])
@auth.login_required
def create_item():
    """" create item """
    if not request.json or not 'title' in request.json:
        abort(400)
    item = {
        'id': items[-1]['id'] + 1,
        'title': request.json['title'],
        'description': request.json.get('description', ""),
        'done': False
    }
    items.append(item)
    return jsonify({'item': [make_public_item(item)]}), 201

# UPDATE
@app.route('/items/<int:item_id>', methods=['PUT'])
@auth.login_required
def update_item(item_id):
    """ update item """
    item = [item for item in items if item['id'] == item_id]
    if len(item) == 0:
        abort(404)
    if not request.json:
        abort(400)
    if 'title' in request.json and type(request.json['title']) != unicode:
        abort(400)
    if 'description' in request.json and type(request.json['description']) is not unicode:
        abort(400)
    if 'done' in request.json and type(request.json['done']) is not bool:
        abort(400)
    item[0]['title'] = request.json.get('title', item[0]['title'])
    item[0]['description'] = request.json.get('description', item[0]['description'])
    item[0]['done'] = request.json.get('done', item[0]['done'])

    return jsonify({'item': [make_public_item(item[0])]})

# DELETE
@app.route('/items/<int:item_id>', methods=['DELETE'])
@auth.login_required
def delete_item(item_id):
    """ delete item """
    item = [item for item in items if item['id'] == item_id]
    if len(item) == 0:
        abort(404)
    items.remove(item[0])
    return jsonify({'result': True})

# ERRORS
@app.errorhandler(400)
def bad_request(error):
    """ bad request """
    return make_response(jsonify({'error': 'Bad Request'}), 400)

@auth.error_handler
def unauthorized():
    """ unauth """
    return make_response(jsonify({'error': 'Unauthorized Access'}), 401)

@app.errorhandler(404)
def not_found(error):
    """ not found """
    return make_response(jsonify({'error': 'Not Found'}), 404)

# RUN
if __name__ == '__main__':
    db_init()
    app.run(debug=True)
